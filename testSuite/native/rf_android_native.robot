*** Settings ***
Documentation    Suite description
Library  AppiumLibrary
Library  DateTime
Library  OperatingSystem

*** Variables ***
${TYPE OF FILE}  png
${ANDROID_AUTOMATION_NAME}    UIAutomator2
${ANDROID_APP}                ${EXECDIR}/apk/AUT.apk
${ANDROID_PLATFORM_NAME}      Android
${ANDROID_PLATFORM_VERSION}   6
${EMAIL_TEST}  success@envato.com
${PASSWORD_TEST}  password

*** Keywords ***
Launch Android Application
     Open Application  http://127.0.0.1:4723/wd/hub  automationName=${ANDROID_AUTOMATION_NAME}
     ...  platformName=${ANDROID_PLATFORM_NAME}
     ...  app=${ANDROID_APP}  appPackage=com.example.mkim.aut  appActivity=com.example.mkim.aut.LoginActivity

Execute Test
     ${Date}=  Get DateTime
     Input Text    com.example.mkim.aut:id/email    ${EMAIL_TEST}
     Input Text    com.example.mkim.aut:id/password    ${PASSWORD_TEST}
     Click Element   com.example.mkim.aut:id/email_sign_in_button
     Wait Until Page Contains Element  com.example.mkim.aut:id/Logout
     Element Should Be Visible  com.example.mkim.aut:id/Logout
     Click Element   com.example.mkim.aut:id/Logout
     Wait Until Element Is Visible  com.example.mkim.aut:id/email
     Do Screenshot  nativeAndroid_${Date}
     Quit Application

Get DateTime
  ${date1}=  Get Current Date  result_format=%Y-%m-%d %H-%M-%S
  [Return]   ${date1}

Do Screenshot
  [Arguments]  ${filename}
  Capture Page Screenshot  ${filename}.${TYPE OF FILE}
  Move File   ${filename}.${TYPE OF FILE}  ./screenshots/
  Log To Console  ${\n}Screenshot

*** Test Cases ***
Execute Android APK
    [Tags]	    native
    [Documentation]  Execute Android Application
    Launch Android Application
    Execute Test