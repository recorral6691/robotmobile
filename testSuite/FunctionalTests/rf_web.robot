*** Settings ***
Library  SeleniumLibrary
Library  OperatingSystem
*** Settings ***
Library  SeleniumLibrary
Library  OperatingSystem
Library  DateTime

*** Variables ***
 @{navegadores}  chrome  firefox
 ${TYPE OF FILE}  png

*** Keywords ***
Abrir navegador chrome
    Open browser    http://blazedemo.com/   chrome

Abrir navegador firefox
    Open browser    http://blazedemo.com/   firefox

Selecciona Origen y destino
    Select From List By Value   xpath://select[@name='fromPort']  Paris
    Select From List by Value   xpath://select[@name='toPort']    London

Envia peticion
   Click Button    css:input[type='submit']

Valida el contenido
    @{flights}=  Get WebElements    css:table[class='table']>tbody tr
    Should Not Be Empty     ${flights}

Cerrar todos los navegadores
     Close All Browsers

Get DateTime
  ${date1}=  Get Current Date  result_format=%Y-%m-%d %H-%M-%S
  [Return]   ${date1}

Do Screenshot
  [Arguments]  ${filename}
  Capture Page Screenshot  ${filename}.${TYPE OF FILE}
  Move File   ${filename}.${TYPE OF FILE}  ./screenshots/
  Log To Console  ${\n}Screenshot



*** Test Cases ***
The user can search for flights in chrome
    [Tags]	    functional
    ${Date}=  Get DateTime
    Abrir navegador chrome
    Selecciona Origen y destino
    Envia peticion
    Do Screenshot  web_${Date}
    Valida el contenido
    Do Screenshot  web_${Date}
